#include <kernel.h>

void sig_ej1_handler(void){
	cprintk(LIGHTGREEN,BLACK,"Manejando la signal 27...\n");
}

void sig_ej2_handler(void){
	cprintk(LIGHTGREEN,BLACK,"Manejando la signal 29...\n");
	cprintk(LIGHTGREEN,BLACK,"Seteando de nuevo el manejador de la signal 29...\n");
	signal(29,sig_ej2_handler);
}

int sig_one_main(void){
	bool cursor;
	mt_cons_clear();
	cursor = mt_cons_cursor(true);
	cprintk(LIGHTGREEN, BLUE,"Bienvenido a sig_one\n\n");
	printk("-Al recibir la signal 1 morire\n");
	printk("-Al recibir la signal 2 la no hare nada\n");
	printk("-Las signals 3 4 5 6 7 8 9 10 11 han sido enmascarada asi que seran ignoradas\n");
	printk("-La signal 27 tiene un manejador asosiado, pero este no es setteado nuevamente\n");
	printk("-La signal 29 tiene un manejador asosiado, y este es setteado nuevamente\n");
	printk("-Puede referirse a esta tarea como:");
	cprintk(LIGHTBLUE, BLACK, " %p\n",mt_curr_task);
	printk("\n\n");
	signal(27,sig_ej1_handler);
	signal(29,sig_ej2_handler);
	setsigmask(2044);
	while(1){		
		printk("Esperando una signal...\n");
		pause();
		cprintk(GREEN, BLACK, "Me llego una signal y ya la maneje!\n\n");			
	}
	mt_cons_clear();
	mt_cons_cursor(cursor);	
}
